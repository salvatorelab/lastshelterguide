import * as riot from 'riot'
import Challenges from './challenges.riot'
import Hero from './hero.riot'

var moment = require('moment');

riot.register('challenges', Challenges);
riot.register('hero', Hero);

riot.mount('challenges');
riot.mount('hero');
